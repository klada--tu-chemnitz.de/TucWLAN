package de.tu_chemnitz.wlan;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.tu_chemnitz.wlan.Helper.API_URL;
import static de.tu_chemnitz.wlan.Helper.API_USER_URL;
import static de.tu_chemnitz.wlan.Helper.username;

public class LoginActivity extends AppCompatActivity {

    private boolean ignoreNext = false;
    private boolean sawLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Helper.setActivity(this);

        WebView loginView = (WebView)findViewById(R.id.webView_login);
        loginView.getSettings().setJavaScriptEnabled(true);
        //loginView.getSettings().setDomStorageEnabled(true);
        //loginView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);



        loginView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                checkAPIUrl(url);
            }

            @SuppressWarnings("deprecation")
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                return checkUrl(uri);
            }

            @TargetApi(Build.VERSION_CODES.N)
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return checkUrl(request.getUrl());
            }

            /*@SuppressWarnings("deprecation")
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                Uri uri = Uri.parse(url);
                checkUrl(uri);
                return null;
            }

            @TargetApi(Build.VERSION_CODES.N)
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                checkUrl(request.getUrl());
                return null;
            }*/
        });

        getConfig();
    }

    private void checkAPIUrl(String str) {
        if (str.contains(API_URL)) {
            Log.d("checkUrl", "in API check " + str);
            //WebView wv = (WebView)findViewById(R.id.webView_login);
            CookieSyncManager.getInstance().sync();
            CookieManager cm = CookieManager.getInstance();
            Log.d("checkUrl", "got cookie manager");
            String c = null;
            if (cm.hasCookies()) {
                Log.d("checkAPIUrl", "has cookies");
                c = cm.getCookie(API_URL);
                Log.d("checkUrl", "got cookie");
            }
            if (c == null) c = "";

            Log.d("checkUrl", "Cookies: " + c);

            Map<String, List<String>> map = new HashMap<>();
            map.put("set-cookie", Arrays.asList(c.split(";")));

            Helper.setCookies(map);

            getConfig();
        } else {
            Log.d("checkUrl", "not in API check "+ str);
        }
    }

    private boolean checkUrl(Uri url) {
        String str = url.toString();
        Log.d("checkUrl", str);

        // don't check cookies here! this breaks below android 5
        //checkAPIUrl(str);

        // don't show API (but redirect to login)
        if (str.contains(API_URL) && !sawLogin) {
            sawLogin = true;
            return false;
        }

        // following ensures user does not leave the shibboleth login
        if (str.contains(".js") ||
                str.contains(".css") ||
                str.contains(".png") ||
                str.contains(".svg") ||
                str.contains(".gif") ||
                str.contains(".ico") ||
                str.contains("hibboleth") ||
                str.contains("krb") ||
                str.contains("sso") ||
                str.contains("?") ||
                str.contains(".woff2")) // font for captcha and stuff
            return false;

        Log.d("checkUrl", "Going to stop loading...");
        return true;
        /*LoginActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((WebView)findViewById(R.id.webView_login)).stopLoading();
            }
        });*/
    }

    private void getConfig() {
        Helper.buildRequest(this.getApplicationContext(), API_USER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                username = Helper.getUsername(response);
                if (username == null) {
                    // don't immediately try again, let the user sign in
                    if (ignoreNext) {
                        ignoreNext = false;
                        return;
                    }
                    ignoreNext = true;
                    ((WebView)findViewById(R.id.webView_login)).loadUrl(API_URL);
                } else {
                    if (!Helper.buildFromPreferences(LoginActivity.this)) {
                        // we have no device password -> get one
                        Helper.buildRequest(LoginActivity.this.getApplicationContext(), API_URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if (Helper.buildWifiConfig(response)) {
                                    //showText("Konfiguration erfolgreich geladen.<br>Warte auf Freigabe von Server.");

                                    SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("de.tu_chemnitz.de.wlan.config", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPrefs.edit();
                                    editor.putString("wifi_json", response);
                                    editor.apply();

                                    setResult(Activity.RESULT_OK);

                                    //waitForServer(1);
                                } else {
                                    //showText("Fehler!<br>Irgendetwas ist schief gelaufen...<br>" + TRY_AGAIN_HTML);
                                    //reset();
                                    setResult(Activity.RESULT_CANCELED);
                                }
                                finish();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("LoginActivity", "getConfig", error);
                                setResult(Activity.RESULT_CANCELED);
                                finish();
                            }
                        }, Request.Method.POST);
                    } else {
                        // we have password and login -> quit login screen
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LoginActivity", "getConfig", error);
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        }, Request.Method.GET);
    }
}
