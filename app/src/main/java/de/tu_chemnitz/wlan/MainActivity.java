package de.tu_chemnitz.wlan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static de.tu_chemnitz.wlan.Helper.username;

public class MainActivity extends AppCompatActivity {

    static class TickLine {
        TickLine(boolean ticked, String text, String subtext, boolean subentry) {
            this.ticked = ticked;
            this.text = text;
            this.subtext = subtext;
            this.subentry = subentry;
        }
        String text, subtext, _tag;
        boolean ticked, subentry, working;

        @Override
        public String toString() {
            return "ticked: " + this.ticked + "; working: " + this.working + "; text: " + this.text + "; subtext: " + this.subtext + "; subentry: " + this.subentry + "; _tag: " + this._tag;
        }
    }

    private static int REQUEST_CODE_LOGIN = 0;

    public static WeakReference<ConnectionTestActivity> connectionTest;

    List<TickLine> ticks_status;
    Timer timer, timer_install;
    boolean inInstall = false, canLeaveInstall = false;
    int useCache = 0; // <0 - false ; 0 - undefined ; >0 - true

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        Intent intent = getIntent();

        String action = intent.getAction();
        //String type = intent.getType();

        if (Intent.ACTION_VIEW.equals(action)) {
            // we got some data, we assume it is somehow valid...
            try {
                if (!intent.getData().toString().startsWith("file:")) {
                    // we got no URI to local device ; fetch over network
                    Helper.buildRequest(this, intent.getDataString().toString(), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Helper.quickApply(MainActivity.this, response);
                        }
                    }, Request.Method.GET);
                } else {
                    // we have a file ; read it
                    InputStream stream = getContentResolver().openInputStream(intent.getData());
                    BufferedReader r = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Helper.quickApply(this, total.toString());
                }
            } catch (Exception e) {
                Toast.makeText(this, "Konnte Datei nicht lesen oder ungültiges Format!", Toast.LENGTH_LONG).show();
                Log.e("ACTION_VIEW - handler", "Something went wrong!", e);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        recomputeLayout();
    }

    private void recomputeLayout() {

        if (inInstall) {

            Helper.getInstallStatus(this);

            timer = new Timer(true);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            redrawLayoutInstall();
                            if (Helper.ticks_install.size() >= 4)
                                timer.cancel();
                        }
                    });
                }
            }, 1000, 2000);

        } else {

            recomputeLayoutStatus();
            redrawLayoutStatus();

        }
    }

    private void recomputeLayoutStatus() {
        ticks_status = Helper.getNetworkStatus(MainActivity.this, "\"eduroam\"");

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                boolean hasInternet = Helper.hasInternet(MainActivity.this);
                TickLine tick = new TickLine(hasInternet, hasInternet ? "Mit Internet verbunden" : "Kein Internetzugang", "Verbindung", false);
                tick._tag = Helper.status_tags[1];
                ticks_status.add(0, tick);
            }
        });
        t.start();

        KeyguardManager km = (KeyguardManager)MainActivity.this.getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
        boolean isSecure = km.isKeyguardSecure();
        TickLine tick = new TickLine(isSecure, isSecure ? "Sperrbildschirm gesetzt" : "PIN- oder Passwortsperre erforderlich", "", false);
        tick._tag = Helper.status_tags[2];
        ticks_status.add(tick);

        try {
            t.join();
        } catch (InterruptedException e) {
            Log.e("MainActivity", "recomputeLayout", e);
        }
    }

    private void redrawLayoutStatus() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.main);
        ll.removeAllViews();
        ll.setPadding(0,0,0,0);

        boolean valid = true;
        for (String tag : Helper.status_tags) {
            TickLine tick = Helper.getByTag(ticks_status, tag);
            valid &= (tick != null && tick.ticked);
        }

        boolean configured = true;
        for (TickLine tick : ticks_status) {
            // ALL ticks need to be there (eduroam completely configured)
            configured &= (tick != null && tick.ticked);
        }

        Button btn = new Button(this);
        btn.setText(valid ? "Konfiguration " + (configured ? "erneut " : "") + "starten" : "Fehler lösen");
        btn.setTextSize(18);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_start_click();
            }
        });
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 20);
        ll.addView(btn, params);

        ImageView iv = new ImageView(this);
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);
        params.setMargins(10, 10, 10, 10);
        iv.setBackgroundColor(Color.GRAY);
        ll.addView(iv, params);

        for (TickLine tick : ticks_status) {
            addTick(tick);
        }
    }

    private void redrawLayoutInstall() {
        LinearLayout ll = (LinearLayout)findViewById(R.id.main);

        ll.removeAllViews();
        ll.setPadding(0,20,0,0);

        for (String tag : Helper.install_order) {
            TickLine tick = Helper.getByTag(Helper.ticks_install, tag);
            if (tick != null)
                addTick(tick);
        }

        if (canLeaveInstall) {
            Button btn = new Button(this);
            btn.setText("Verlassen");
            btn.setTextSize(18);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btn_exit_click();
                }
            });
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 20, 0, 0);
            ll.addView(btn, params);
        }
    }

    private void btn_exit_click() {
        inInstall = false;
        recomputeLayout();
    }

    private void btn_start_click() {
        useCache = 0;
        Helper.device_name = "";
        setupEduroam();
    }

    private void setupEduroam() {
        boolean valid = true;
        for (String tag : Helper.status_tags) {
            TickLine tick = Helper.getByTag(ticks_status, tag);
            valid &= (tick != null && tick.ticked);
        }

        if (!valid) {
            TickLine tick = Helper.getByTag(ticks_status, Helper.status_tags[0]);
            if (tick == null || !tick.ticked) { // wifi disabled
                Helper.enableWifi(this);
                recomputeLayout();
                return;
            }

            tick = Helper.getByTag(ticks_status, Helper.status_tags[2]);
            if (tick == null || !tick.ticked) { // no lockscreen
                AlertDialog alert = new AlertDialog.Builder(this).create();
                alert.setTitle("Sperrbildschirm");

                /*LinearLayout ll = new LinearLayout(this);
                ll.setOrientation(LinearLayout.VERTICAL);

                TextView tv = new TextView(this);
                tv.setText("Bitte setze dir einen PIN- oder Passwort-Sperrbildschirm.");

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 0, 10, 0);
                ll.addView(tv, params);
                builder.setView(ll);*/

                alert.setMessage("Bitte setze dir einen PIN- oder Passwort-Sperrbildschirm.");
                alert.setCancelable(false);
                alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
                    }
                });
                //alert.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));
                alert.show();
                return;
            }

            tick = Helper.getByTag(ticks_status, Helper.status_tags[1]);
            if (tick == null || !tick.ticked) { // no internet
                AlertDialog alert = new AlertDialog.Builder(this).create();
                alert.setTitle("Internetzugriff");

                /*LinearLayout ll = new LinearLayout(this);
                ll.setOrientation(LinearLayout.VERTICAL);

                TextView tv = new TextView(this);
                tv.setText("Bitte stelle eine Internetverbindung mit einem WLAN oder über mobile Daten her.\n" +
                        "In der TU Chemnitz kannst du die WLAN Netze 'camo' oder 'tu-chemnitz.de' verwenden.");

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 0, 10, 0);
                ll.addView(tv, params);
                builder.setView(ll);*/

                alert.setMessage("Bitte stelle eine Internetverbindung mit einem WLAN oder über mobile Daten her.\n" +
                        "In der TU Chemnitz kannst du die WLAN Netze 'camo' oder 'tu-chemnitz.de' verwenden.");
                alert.setCancelable(false);
                alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                //alert.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));
                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        recomputeLayout();
                    }
                });
                alert.show();
            }
        } else {
            // everything is set up -> start configuration!
            canLeaveInstall = false;
            inInstall = true;
            if (Helper.ticks_install == null) {
                Helper.getInstallStatus(this);
            }

            TickLine got_config = Helper.getByTag(Helper.ticks_install, Helper.install_order[1]);

            boolean hasConfig = got_config != null && got_config.ticked;

            if (hasConfig && useCache == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Neue Konfiguration anfordern?");

                builder.setMessage("Es wurde bereits eine Konfiguration geladen, soll diese verwendet werden?");

                // Set up the buttons
                builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        useCache = 1;
                        setupEduroam();
                    }
                });
                builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        useCache = -1;
                        Helper.deletePreferences(MainActivity.this);
                        Helper.ticks_install = null;
                        setupEduroam();
                    }
                });

                builder.show();
                return;
            }

            if (useCache == 0) {
                useCache = 1;
            }

            if (Helper.device_name.equals("") && (!hasConfig || useCache < 0)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Gerätename");

                LinearLayout ll = new LinearLayout(this);
                ll.setOrientation(LinearLayout.VERTICAL);

                TextView tv = new TextView(this);
                tv.setText("Bitte gib einen Namen für dieses Gerät an!\nDer Name kann später verwendet werden um das gerätespezifische Passwort im IdM-Portal zu verwalten.");

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 0, 10, 0);
                ll.addView(tv, params);

                // Set up the input
                final EditText input = new EditText(this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                if (Helper.device_name.equals(""))
                    input.setText(Build.MANUFACTURER + " - " + Build.MODEL + " (API " + Build.VERSION.SDK_INT + ")");
                else
                    input.setText(Helper.device_name);
                ll.addView(input, params);
                builder.setView(ll);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Helper.device_name = input.getText().toString();
                        setupEduroam();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                // shows cursor - may fix #12
                input.requestFocus();
                // maybe add automatic keyboard display? (#12)
                /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);*/
            } else {
                TickLine login = null;
                int i = 0;
                while (login == null && i++ < 100)
                    try {
                        login = Helper.getByTag(Helper.ticks_install, Helper.install_order[0]);
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        Log.e("MainActivity", "btn_start_click", e);
                    }
                if (login == null) {
                    AlertDialog alert = new AlertDialog.Builder(this).create();
                    alert.setTitle("Loginfehler");

                    alert.setMessage("Konnte nicht feststellen ob Benutzer eingeloggt ist, bitte später erneut versuchen.");
                    alert.setCancelable(false);
                    alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            inInstall = false;
                            recomputeLayout();
                        }
                    });
                    alert.show();
                    return;
                }
                if (!login.ticked || (got_config == null || !got_config.ticked)) {
                    // open login
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivityForResult(intent, REQUEST_CODE_LOGIN);
                    return;
                }

                timer_install = new Timer(true);
                timer_install.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Helper.getInstallStatus(MainActivity.this);
                        int i = 0;
                        TickLine isReady = null;
                        while (isReady == null && i++ < 100) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                Log.e("MainActivity", "timer_install", e);
                            }
                            isReady = Helper.getByTag(Helper.ticks_install, Helper.install_order[2]);
                        }
                        if (isReady != null)
                            if (isReady.ticked) {
                                boolean res = Helper.applyWifiConfig(MainActivity.this);
                                TickLine completed = Helper.getByTag(Helper.ticks_install, Helper.install_order[3]);
                                if (completed != null) {
                                    completed.ticked = res;
                                    completed.text = res ? "Installation abgeschlossen!" : "Fehlgeschlagen, löschen Sie ggf. bestehende eduroam Konfiguration!";
                                }
                                MainActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        canLeaveInstall = true;
                                        redrawLayoutInstall();
                                    }
                                });
                                if (res) {
                                    postInstallCheck();
                                }
                                timer_install.cancel();
                            } else if (!isReady.working) {
                                TickLine completed = Helper.getByTag(Helper.ticks_install, Helper.install_order[3]);
                                if (completed != null) {
                                    completed.text = "Nicht ausführbar!";
                                }
                                Helper.deletePreferences(MainActivity.this);
                                MainActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        canLeaveInstall = true;
                                        redrawLayoutInstall();
                                    }
                                });
                                timer_install.cancel();
                            }
                    }
                }, 100, 5000);

            }
        }
    }

    private void postInstallCheck() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Verbindungsprüfung");

                LinearLayout ll = new LinearLayout(MainActivity.this);
                ll.setOrientation(LinearLayout.VERTICAL);

                TextView tv = new TextView(MainActivity.this);
                tv.setText(
                        "Soll jetzt zu \"eduroam\" verbunden werden?\nDafür müssen Sie in Reichweite des WLANs sein (in Uninähe).\n" +
                        "Die Verbindungsprüfung verläuft im Hintergrund und kann bis zu einer Minute dauern.");

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 0, 10, 0);
                ll.addView(tv, params);

                builder.setView(ll);

                // inner dialog
                final AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
                alert.setTitle("Verbindungstest");
                alert.setCancelable(false);
                alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                        Context context = MainActivity.this.getApplicationContext();
                        WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
                        boolean res = wifi.enableNetwork(Helper.lastAddedWifi, true);
                        Log.d("postInstallCheck", "enableNetwork: " + res);

                        if (res) {

                            ConnectionTestActivity.timer = new Timer(true);
                            ConnectionTestActivity.receiver = new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {

                                    /*int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);

                                    switch (wifiState) {
                                        case WifiManager.WIFI_STATE_ENABLED:
                                            // go on
                                            Log.d("postInstallCheck", "got some network");
                                            break;
                                        default:
                                            // do nothing
                                            Log.d("postInstallCheck", "no new network");
                                            return;
                                    }*/

                                    ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                                    NetworkInfo info = connectivityManager.getActiveNetworkInfo(); // intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                                    if (info != null && info.isConnected()) {
                                        WifiManager wifiManager = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                                        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                                        String ssid = wifiInfo.getSSID();

                                        Log.d("postInstallCheck", "connected to Wifi: " + ssid);

                                        if ("\"eduroam\"".equals((ssid))) {
                                            connectionTest.get().btn_cancel_Click(null);
                                            Thread t = new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    boolean res = Helper.hasInternet(MainActivity.this.getApplicationContext());
                                                    Log.d("postInstallCheck", "hasInternet: " + res);
                                                }
                                            });
                                            t.start();

                                            try {
                                                t.join();
                                            } catch (InterruptedException e) {
                                                Log.e("postInstallCheck", "hasInternet join", e);
                                            }

                                            alert.setMessage(Helper.lastHasInternet ?
                                                    "Verbindung erfolgreich hergestellt!" :
                                                    "Kein Internetzugriff.\nProbieren Sie es später erneut.");

                                            alert.show();
                                        } else {
                                            // currently unused
                                            alert.setMessage("Konnte eduroam nicht finden.\nProbieren Sie es später erneut.");
                                        }
                                    } else {
                                        // currently unused
                                        alert.setMessage("Konnte keine Verbindung herstellen.\nProbieren Sie es später erneut.");
                                    }
                                }
                            };

                            ConnectionTestActivity.timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    MainActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            connectionTest.get().btn_cancel_Click(null);
                                            alert.setMessage(
                                                    "Konnte kein \"eduroam\" finden.\nSie müssen sich in Reichweite des WLAN-Zugangs (in Uninähe) befinden.\n" +
                                                            "Versuchen Sie es später erneut.");

                                            alert.show();
                                        }
                                    });
                                }
                            }, 30000);

                            startActivity(new Intent(MainActivity.this, ConnectionTestActivity.class));

                            // change tab to status (many red texts in install tab, looks like something went wrong)
                            /*TabHost host = (TabHost)findViewById(R.id.tab_host);
                            host.setCurrentTab(0);*/
                            inInstall = false;
                            recomputeLayout();

                            // throws NullPointerException
                            //connectionTest.get().registerReceiver(ConnectionTestActivity.receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                        } else {
                            alert.setMessage("Konnte keine Verbindung herstellen.\nProbieren Sie es später erneut.");
                            alert.show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_LOGIN) {
            if (resultCode == Activity.RESULT_OK) {
                TickLine login = Helper.getByTag(Helper.ticks_install, Helper.install_order[0]);
                if (login != null) {
                    login.text = "Angemeldet als " + username;
                    login.ticked = true;
                }
                TickLine got_config = Helper.getByTag(Helper.ticks_install, Helper.install_order[1]);
                if (got_config != null) {
                    got_config.text = "Konfiguration erhalten";
                    got_config.ticked = true;
                }
                redrawLayoutInstall();
                setupEduroam();
            } else {
                // resultCode == Activity.RESULT_CANCELED
                AlertDialog alert = new AlertDialog.Builder(this).create();
                alert.setTitle("Loginfehler");

                alert.setMessage("Falls Sie den Login nicht abgebrochen haben, versuchen sie es bitte erneut, es scheint ein Fehler aufgetreten zu sein.");
                alert.setCancelable(false);
                alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                alert.show();
                inInstall = false;
                recomputeLayout();
            }
        }
    }

    private void addTick(TickLine line) {
        ((LinearLayout)findViewById(R.id.main)).addView(getTickLine(line));
    }

    private View getTickLine(TickLine line) {

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.HORIZONTAL);


        ImageView iv = new ImageView(this);
        iv.setImageResource(R.drawable.ic_tick);
        if (!line.ticked)
            iv.setVisibility(View.INVISIBLE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 20, 0);
        ll.addView(iv, params);


        if (!line.ticked && line.working) {
            Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_tick);
            ProgressBar spinner = new ProgressBar(this);
            spinner.setIndeterminate(true);
            spinner.getIndeterminateDrawable().setColorFilter(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null), PorterDuff.Mode.MULTIPLY);
            params = new LinearLayout.LayoutParams(d.getIntrinsicWidth(), d.getIntrinsicHeight());
            params.setMargins(0, 0, 20, 0);
            ll.addView(spinner, params);
            iv.setVisibility(View.GONE);
        }


        LinearLayout ll2 = new LinearLayout(this);
        ll2.setOrientation(LinearLayout.VERTICAL);
        ll2.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);


        TextView tv = new TextView(this);
        tv.setText(line.text);
        tv.setTextColor(line.ticked ? ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null) : Color.RED);
        ll2.addView(tv);


        tv = new TextView(this);
        tv.setText(line.subtext);
        ll2.addView(tv);

        iv = new ImageView(this);
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);
        iv.setBackgroundColor(Color.GRAY);
        ll2.addView(iv, params);

        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ll.addView(ll2, params);
        
        ll.setLayoutParams(getLayoutParams(line.subentry));
        
        return ll;
    }

    private LinearLayout.LayoutParams getLayoutParams(boolean subentry) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins((subentry ? 50 : 0), 0, 0, 10);
        return params;
    }
}
