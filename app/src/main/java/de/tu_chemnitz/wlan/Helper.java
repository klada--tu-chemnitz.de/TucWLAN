/**
 * Parts of this code are from
 * http://www.programcreek.com/java-api-examples/index.php?source_dir=android-sdk-sources-for-api-level-23-master/com/android/connectivitymanagertest/WifiConfigurationHelper.java
 * which was distributed under Apache License, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * by The Android Open Source Project (Copyright (C) 2010)
 */

package de.tu_chemnitz.wlan;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiEnterpriseConfig;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class Helper {

    //private static int VERSION = 1;
    //static String API_URL = "https://tom.hrz.tu-chemnitz.de/mouse/user/service/account/devicepasswords/api/"; // DEBUG
    static String API_URL = "https://idm.hrz.tu-chemnitz.de/user/service/account/devicepasswords/api/"; // PRODUCTION
    static String API_USER_URL = "https://idm.hrz.tu-chemnitz.de/js/current_user/";
    private static Response.ErrorListener commonErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            // ignore errors for now
            //if (error.networkResponse.statusCode == 405) // method not allowed
            Log.e("commonErrorListener", "Network error", error);
            //activity.showText("<b>Netzwerkfehler!</b><br>Irgendetwas ist schief gelaufen.<br>" + LoginActivity.TRY_AGAIN_HTML);
        }
    };

    //private static LoginActivity activity;

    //static String lastReceivedToken = null;
    static String checkURL = null;
    static String username = null;
    private static Map<String, List<String>> cookies; // session cookies go here
    private static CookieManager cookieManager = new CookieManager();
    private static int primaryKey = -1;
    static int lastAddedWifi = -1;
    static WifiConfiguration lastWifi = null;

    static boolean lastHasInternet = false;
    static List<MainActivity.TickLine> ticks_install = null;
    //static ReentrantLock install_list_lock = new ReentrantLock();

    static String[] status_tags = { // used to verify all needed for install is set
            "wifiEnabled",
            "hasInternet",
            "isSecure"
    };

    static String[] install_order = { // used for display order & later UI changes
            "checkWTCLogin",
            "gotPassword",
            "validPassword",
            "ready"
    };

    static String device_name = "";

    /*static void setActivity(LoginActivity a) {
        activity = a;
    }*/

    static void setCookies(Map<String, List<String>> map) {
        cookies = map;
        CookieHandler.setDefault(cookieManager);
    }

    static void resetCookies() {
        cookies = null;
        cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);
    }

    static void buildRequest(Context context, String url, Response.Listener<String> listener, final int method) {
        buildRequest(context, url, listener, commonErrorListener, method);
    }

    static void buildRequest(Context context, String url, Response.Listener<String> listener, Response.ErrorListener error, final int method) {
        RequestQueue queue = Singleton.getInstance(context).getRequestQueue();

        if (cookies != null)
            try {
                URI uri = new URI(url);
                cookieManager.put(uri, cookies);
            } catch (Exception e) {
                Log.e("buildRequest", "Exception", e);
            }

        StringRequest request;

        // new style
        final String requestBody;
        try {
            requestBody = "device_name=" + URLEncoder.encode(device_name, "utf-8");
        } catch (UnsupportedEncodingException uee) {
            Log.e("newStyle", "unsupportedEncoding", uee);
            return;
        }

        request = new StringRequest(
                method,
                url,
                listener,
                error) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null || method != Method.POST ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

        };

        /*
        // old style
        request = new StringRequest(
                Request.Method.POST,
                url,
                listener,
                commonErrorListener) {

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("device_name", device_name);
                params.put("protocol", "" + VERSION);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }

        };*/

        queue.add(request);
    }

    static MainActivity.TickLine getByTag(List<MainActivity.TickLine> list, String tag) {
        if (list == null)
            return null;
        for (MainActivity.TickLine tick : list) {
            if (tick != null && tag.equals(tick._tag))
                return tick;
        }
        return null;
    }

    /*static void getDeviceInfoPOSTString(Map<String, String> map) {
        //map.put("model", Build.MODEL);
        //map.put("version", Build.VERSION.RELEASE);
        //map.put("manufacturer", Build.MANUFACTURER);
        //map.put("versionName", Build.VERSION.CODENAME);
        map.put("device_name", device_name);
        map.put("protocol", "" + VERSION);

        //String ret = "model=" + Build.MODEL;
        //ret += "&version=" + Build.VERSION.RELEASE;
        //ret += "&manufacturer=" + Build.MANUFACTURER;
        //ret += "&versionName=" + Build.VERSION.CODENAME;
        //return ret;
    }*/

    /*static boolean reset(String json) {
        try {
            JSONObject jObj = new JSONObject(json);
            if (!jObj.has("type") || !jObj.getString("type").equals("wifi_delete_config")) {
                return false;
            }

            if (!jObj.has("version") || jObj.getInt("version") != VERSION) {
                return false;
            }

            if (jObj.getBoolean("success")) {
                clearValues();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    static void clearValues() {
        lastWifi = null;
        lastAddedWifi = -1;
        //lastReceivedToken = null;
        checkURL = null;
        primaryKey = -1;
    }*/

    static void deletePreferences(Context context) {
        SharedPreferences sharedPrefs = context.getApplicationContext().getSharedPreferences("de.tu_chemnitz.de.wlan.config", Context.MODE_PRIVATE);
        if (sharedPrefs.contains("wifi_json")) {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.remove("wifi_json");
            editor.apply();
        }
    }

    static boolean buildFromPreferences(Context context) {
        SharedPreferences sharedPrefs = context.getApplicationContext().getSharedPreferences("de.tu_chemnitz.de.wlan.config", Context.MODE_PRIVATE);
        return  sharedPrefs.contains("wifi_json") &&
                buildWifiConfig(sharedPrefs.getString("wifi_json", "{}")); // default is empty JSON {}
    }

    static boolean buildWifiConfig(String json) {
        try {
            Log.d("buildWifiConfig", json);
            JSONObject jObj = new JSONObject(json);
            if (!jObj.has("type") || !jObj.getString("type").equals("wifi_config")) {
                return false;
            }
            // we just assume that we got everything we need (type is what it should be)

            // seems unused/unneeded
            /*if (!jObj.has("version") || jObj.getInt("version") != VERSION) {
                return false;
            }*/

            // if username mismatches we can't fetch deploy_status -> abort
            if (!jObj.has("username") || username == null || !username.equals(jObj.getString("username"))) {
                return false;
            }

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            JSONArray certs = jObj.getJSONArray("certs");
            X509Certificate[] x509s = new X509Certificate[certs.length()];
            for (int i = 0; i < certs.length(); i++) {
                x509s[i] = (X509Certificate)cf.generateCertificate(
                        new ByteArrayInputStream(
                                Base64.decode(certs.getString(i), Base64.DEFAULT)));
            }

            WifiConfiguration conf = createEapConfig(
                    jObj.getString("ssid"),
                    jObj.getString("password"),
                    jObj.getString("eapMethod"),
                    (jObj.isNull("phase2") ? null : jObj.getString("phase2")),
                    jObj.getString("identity"),
                    (jObj.isNull("anonymousIdentity") ? null : jObj.getString("anonymousIdentity")),
                    jObj.getString("subjectMatch"),
                    x509s); // X509Cert

            //lastReceivedToken = jObj.getString("token");
            checkURL = jObj.getString("deploy_status");
            primaryKey = jObj.getInt("pk");
            lastWifi = conf;

            return true;

        } catch (Exception e) {
            Log.e("buildWifiConfig", "Exception", e);

            return false;
        }
    }

    static int getReadyState(String json) {
        /* @return 0 - pending
         * @return <0 - failure
         * @return >0 - success
         */
        try {
            JSONObject jObj = new JSONObject(json);
            /* deploy_state
             * 1 - finished
             * 2 - pending
             * 4 - deploying
             * 5 - failed
             */
            int deploy_state = (jObj.has("deploy_state") ? jObj.getInt("deploy_state") : -1);
            /* state
             * 10 - active
             * 20 - locked
             * 30 - expired
             * 40 - deleted
             */
            int state = (jObj.has("state") ? jObj.getInt("state") : -1);
            if (deploy_state < 0 || state < 0 ||
                    deploy_state == 5 || state >= 20)
                return -1;
            if (deploy_state == 1 && state == 10)
                return 1;
            return 0;
        } catch (Exception e) {
        }
        return -1;
    }

    /*static boolean checkConfigReadyOnServer(String json) {
        try {
            JSONObject jObj = new JSONObject(json);
            return (jObj.has("deploy_state") && jObj.getInt("deploy_state") == 1);
        } catch (Exception e) {
            return false;
        }
    }

    static boolean checkConfigFailedOnServer(String json) {
        try {
            JSONObject jObj = new JSONObject(json);
            return (jObj.has("deploy_state") && jObj.getInt("deploy_state") == 5);
        } catch (Exception e) {
            return false;
        }
    }*/

    static String getUsername(String json) {
        try {
            JSONObject jObj = new JSONObject(json);
            if (jObj.has("username"))
                return jObj.getString("username");
        } catch (Exception e) {
        }
        return null;
    }

    static void enableWifi(Context context) {
        WifiManager wifiMan = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (!wifiMan.isWifiEnabled())
            wifiMan.setWifiEnabled(true);

        int i = 0;
        while (!wifiMan.pingSupplicant() && i++ < 100)
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                Log.e("Helper", "enableWifi", e);
            }
    }

    static boolean applyWifiConfig(Context context) {
        try {
            if (lastWifi == null) return false;

            WifiManager wifiMan = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            enableWifi(context);

            int id = -1;
            for (WifiConfiguration conf: wifiMan.getConfiguredNetworks()) {
                if (lastWifi.SSID.equals(conf.SSID)) {
                    id = conf.networkId;
                }
            }
            if (id != -1) {
                Log.d("applyWifiConfig", "updating old eduroam");
                lastAddedWifi = wifiMan.updateNetwork(lastWifi);
            } else {
                lastAddedWifi = wifiMan.addNetwork(lastWifi);
            }

            if (lastAddedWifi == -1) return false;
            Log.d("applyWifiConfig", "Added network");
            return wifiMan.enableNetwork(lastAddedWifi, false) && wifiMan.saveConfiguration();
        } catch (Exception e) {
            Log.e("applyWifiConfig", "Exception", e);

            return false;
        }
    }

    static void quickApply(Context context, String json) {
        if (Helper.buildWifiConfig(json)) {
            if (Helper.applyWifiConfig(context)) {
                Toast.makeText(context, Helper.lastWifi.SSID + " erfolgreich angewendet!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, "Konnte Konfiguration nicht anwenden!\nLöschen Sie ggf. bestehende Konfiguration von " + Helper.lastWifi.SSID + ".", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(context, "Ungültiges Format!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Create a {@link WifiConfiguration} for an EAP secured network
     *
     * @param ssid The SSID of the wifi network
     * @param password The password
     * @param eapMethod The EAP method
     * @param phase2 The phase 2 method or null
     * @param identity The identity or null
     * @param anonymousIdentity The anonymous identity or null
     * // following 2 are marked with @hide so it is not possible to access them from outside the android API
     * //@param caCert The CA certificate or null
     * //@param clientCert The client certificate or null
     * @return The {@link WifiConfiguration}
     */
    private static WifiConfiguration createEapConfig(String ssid, String password, String eapMethod,
                                                    String phase2, String identity, String anonymousIdentity
                                                    /*,String caCert, String clientCert*/
                                                    , String subjectMatch, X509Certificate[] certs) {
        WifiConfiguration config = new WifiConfiguration();
        config.SSID = "\"" + ssid + "\"";

        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);

        Integer _phase2;
        // Set defaults
        if (phase2 == null) _phase2 = WifiEnterpriseConfig.Phase2.NONE;
        else _phase2 = getPhase2(phase2);
        if (identity == null) identity = "";
        if (anonymousIdentity == null) anonymousIdentity = "";
        //if (caCert == null) caCert = "";
        //if (clientCert == null) clientCert = "";

        config.enterpriseConfig.setPassword(password);
        config.enterpriseConfig.setEapMethod(getEapMethod(eapMethod));
        config.enterpriseConfig.setPhase2Method(_phase2);
        config.enterpriseConfig.setIdentity(identity);
        config.enterpriseConfig.setAnonymousIdentity(anonymousIdentity);
        if (Build.VERSION.SDK_INT >= 23)
            config.enterpriseConfig.setAltSubjectMatch("DNS:" + subjectMatch);
        else
            config.enterpriseConfig.setSubjectMatch(subjectMatch);
        // API >= 24 would allow us to set all certificates at once, but let's try to
        // set each certificate individually to possibly work around issue #10
        for (X509Certificate cert : certs) {
            // test if certificate is a root CA
            if (cert.getIssuerX500Principal().equals(cert.getSubjectX500Principal())) {
                Log.d("createEapConfig", "Adding CA certificate " + cert.getSubjectX500Principal().getName());
                config.enterpriseConfig.setCaCertificate(cert);
            }
        }
        //config.enterpriseConfig.setCaCertificateAlias(caCert);
        //config.enterpriseConfig.setClientCertificateAlias(clientCert);
        Log.d("createEapConfig", config.enterpriseConfig.getCaCertificate().getSubjectX500Principal().toString());
        return config;
    }

    /**
     * Get the EAP method from a string.
     *
     * @throws IllegalArgumentException if the string is not a supported EAP method.
     */
    private static int getEapMethod(String eapMethod) {
        if ("TLS".equalsIgnoreCase(eapMethod)) {
            return WifiEnterpriseConfig.Eap.TLS;
        }
        if ("TTLS".equalsIgnoreCase(eapMethod)) {
            return WifiEnterpriseConfig.Eap.TTLS;
        }
        if ("PEAP".equalsIgnoreCase(eapMethod)) {
            return WifiEnterpriseConfig.Eap.PEAP;
        }
        throw new IllegalArgumentException("EAP method must be one of TLS, TTLS, or PEAP");
    }

    /**
     * Get the phase 2 method from a string.
     *
     * @throws IllegalArgumentException if the string is not a supported phase 2 method.
     */
    private static int getPhase2(String phase2) {
        if ("PAP".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.PAP;
        }
        if ("MSCHAP".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.MSCHAP;
        }
        if ("MSCHAPV2".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.MSCHAPV2;
        }
        if ("GTC".equalsIgnoreCase(phase2)) {
            return WifiEnterpriseConfig.Phase2.GTC;
        }
        throw new IllegalArgumentException("Phase2 must be one of PAP, MSCHAP, MSCHAPV2, or GTC");
    }

    static List<MainActivity.TickLine> getNetworkStatus(Context context, String ssid) {

        List<MainActivity.TickLine> list = new ArrayList<>();

        WifiManager wifi = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled())
        {
            MainActivity.TickLine tick = new MainActivity.TickLine(true, "WLAN ist aktiviert", "Verfügbarkeit", false);
            tick._tag = status_tags[0];
            list.add(tick);

            List<WifiConfiguration> currentConfigs = wifi.getConfiguredNetworks();
            for (WifiConfiguration currentConfig : currentConfigs)
            {
                if (ssid.equals(currentConfig.SSID))
                {
                    String message;
                    boolean ticked = true;
                    message=currentConfig.SSID;
                    if (currentConfig.allowedGroupCiphers.toString().contains(",")) message+=" with mixed mode";
                    else if (currentConfig.allowedGroupCiphers.toString().contains("2")) message+=" with CCMP";
                    else if (currentConfig.allowedGroupCiphers.toString().contains("2")) message+=" with TKIP";
                    list.add(new MainActivity.TickLine(ticked, message, "SSID", false));


                    message=currentConfig.enterpriseConfig.getIdentity();
                    ticked = true;
                    list.add(new MainActivity.TickLine(ticked, message, "User Identität", false));


                    if (currentConfig.enterpriseConfig.getAnonymousIdentity().length()>0) {
                        message=currentConfig.enterpriseConfig.getAnonymousIdentity();
                        ticked = true;
                    } else {
                        message="Nicht gesetzt";
                        ticked = false;
                    }
                    list.add(new MainActivity.TickLine(ticked, message, "Anonyme Identität", true));


                    ticked = true;
                    if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.PEAP) message="PEAP with ";
                    else if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.PWD) message="PWD with ";
                    else if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.TLS) message="TLS";
                    else if (currentConfig.enterpriseConfig.getEapMethod()== WifiEnterpriseConfig.Eap.TTLS) message="TTLS with ";
                    else {
                        message="EAP Method error";
                        ticked = false;
                    }
                    if (currentConfig.enterpriseConfig.getEapMethod()!= WifiEnterpriseConfig.Eap.TLS)
                    {
                        if (currentConfig.enterpriseConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.MSCHAPV2) message+="Phase2: MSCHAPv2";
                        else if (currentConfig.enterpriseConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.GTC) message+="Phase2: GTC";
                        else if (currentConfig.enterpriseConfig.getPhase2Method()== WifiEnterpriseConfig.Phase2.PAP) message+="Phase2: PAP";
                        else message+="Phase2 Missing";
                    }
                    list.add(new MainActivity.TickLine(ticked, message, "EAP Methode", true));


                    int nullpoint;
                    nullpoint=currentConfig.enterpriseConfig.toString().indexOf("ca_cert NULL");
                    if (nullpoint==-1){
                        int start,finish;
                        start=currentConfig.enterpriseConfig.toString().indexOf("ca_cert");
                        finish=currentConfig.enterpriseConfig.toString().indexOf("\"", start+10);
                        if (start>0 && finish>0) {
                            message="OK";
                            ticked = true;
                        }
                        else
                        {
                            message="ERROR";
                            ticked = false;
                        }
                    }
                    else
                    {
                        message="Not found";
                        ticked = false;
                    }
                    list.add(new MainActivity.TickLine(ticked, message, "CA Certificate", true));


                    String subjectMatch;
                    if (Build.VERSION.SDK_INT >= 23)
                        subjectMatch = currentConfig.enterpriseConfig.getAltSubjectMatch();
                    else
                        subjectMatch = currentConfig.enterpriseConfig.getSubjectMatch();

                    if (subjectMatch!=null && subjectMatch.length()>0)
                    {
                        message=subjectMatch;
                        ticked = true;
                    }
                    else {
                        message="Missing";
                        ticked = false;
                    }
                    list.add(new MainActivity.TickLine(ticked, message, "Server Subject Match", true));


                    message = "Technische Parameter ";
                    if (list.get(3).ticked && list.get(4).ticked && list.get(5).ticked && list.get(6).ticked) {
                        message += "sicher";
                        ticked = true;
                    } else {
                        message += "unsicher";
                        ticked = false;
                    }
                    list.add(3, new MainActivity.TickLine(ticked, message, "", false));
                }
            }
        } else {
            MainActivity.TickLine tick = new MainActivity.TickLine(false, "WLAN ist deaktiviert", "Verfügbarkeit", false);
            tick._tag = status_tags[0];
            list.add(tick);
        }

        MainActivity.TickLine tick = getByTag(list, status_tags[0]);
        if (list.size() <= 1 && tick != null && tick.ticked) {
            list.add(new MainActivity.TickLine(false, "Kein " + ssid + " konfiguriert", "Verfügbarkeit", false));
        }

        return list;
    }

    static void getInstallStatus(Context context) {

        ticks_install = new ArrayList<>();

        WifiManager wifi = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled() && lastHasInternet)
        {
            buildRequest(context, API_USER_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("getInstallStatus", "Got response");
                    MainActivity.TickLine tick;
                    if (!response.startsWith("{")) {
                        // we got no JSON, we are not logged in
                        tick = new MainActivity.TickLine(false, "Nicht angemeldet", "WTC-Login", false);
                    } else {
                        // logged in && can fetch username
                        username = getUsername(response);
                        if (username == null) {
                            tick = new MainActivity.TickLine(false, "Konnte Benutzer nicht abrufen", "WTC-Login", false);
                        } else {
                            tick = new MainActivity.TickLine(true, "Angemeldet als " + username, "WTC-Login", false);
                        }
                    }
                    tick._tag = install_order[0];
                    Helper.ticks_install.add(tick);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("getInstallStatus", "check user", error);
                    MainActivity.TickLine tick = new MainActivity.TickLine(false, "Fehler", "WTC-Login", false);
                    tick._tag = install_order[0];
                    ticks_install.add(tick);
                }
            }, Request.Method.GET);

            MainActivity.TickLine tick;
            if (buildFromPreferences(context)) {
                tick = new MainActivity.TickLine(true, "Konfiguration erhalten", "Gerätepasswort", false);
            } else {
                tick = new MainActivity.TickLine(false, "Anfrage ausstehend", "Gerätepasswort", false);
            }
            tick._tag = install_order[1];
            ticks_install.add(tick);

            if (tick.ticked) {
                if (checkURL != null) {
                    buildRequest(context, checkURL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            int state = getReadyState(response);
                            MainActivity.TickLine tick;
                            if (state < 0) {
                                tick = new MainActivity.TickLine(false, "Konnte nicht angewendet werden!", "Passwort aktiv", false);
                            } else if (state == 0) {
                                tick = new MainActivity.TickLine(false, "In Arbeit", "Passwort aktiv", false);
                                tick.working = true;
                            } else {
                                tick = new MainActivity.TickLine(true, "OK", "Passwort aktiv", false);
                            }
                            tick._tag = install_order[2];
                            ticks_install.add(tick);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Helper", "getInstallStatus - checkURL", error);
                            if (error.networkResponse != null && error.networkResponse.statusCode == 403) {
                                // Forbidden -> our auth cookies are invalid, somehow?
                                // possibly one user got checkURL -> other user tries to sign in who is not permitted to check this resource
                                resetCookies();
                                MainActivity.TickLine login = getByTag(ticks_install, install_order[0]);
                                if (login != null) {
                                    login.ticked = false;
                                    login.text = "Anmeldung fehlgeschlagen";
                                }
                            }
                            MainActivity.TickLine tick = new MainActivity.TickLine(false, "Netzwerkfehler", "Passwort aktiv", false);
                            tick._tag = install_order[2];
                            ticks_install.add(tick);
                        }
                    }, Request.Method.GET);
                    tick = null;
                } else {
                    tick = new MainActivity.TickLine(false, "Nicht überprüfbar", "Passwort aktiv", false);
                }
            } else {
                tick = new MainActivity.TickLine(false, "Anfrage ausstehend", "Passwort aktiv", false);
            }
            if (tick != null) {
                tick._tag = install_order[2];
                ticks_install.add(tick);
            }

            tick = new MainActivity.TickLine(false, "Ausstehend", "Konfiguration angewendet", false);
            tick._tag = install_order[3];
            ticks_install.add(tick);
        } else {
            MainActivity.TickLine tick = new MainActivity.TickLine(false, "Kein Internetzugriff!", "Verfügbarkeit", false);
            tick._tag = install_order[3];
            ticks_install.add(tick);
        }
    }

    // https://stackoverflow.com/a/6493572
    static boolean hasInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected()) {
            try {
                Log.d("hasIntenet", "hasNetwork");
                HttpURLConnection urlc = (HttpURLConnection)(new URL("http://idm.hrz.tu-chemnitz.de/204.expected")).openConnection();
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();

                //Log.d("hasInternet", "resp: " + urlc.getResponseCode() + " ; length: " + urlc.getContentLength());

                // urlc.getContentLength() can return -1 --> check for <= 0
                return (lastHasInternet = (urlc.getResponseCode() == 204 && urlc.getContentLength() <= 0));
            } catch (Exception e) {
                Log.e("hasInternet", "Exception", e);
            }
        }
        return (lastHasInternet = false);
    }

}
