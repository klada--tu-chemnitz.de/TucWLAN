package de.tu_chemnitz.wlan;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.lang.ref.WeakReference;
import java.util.Timer;

public class ConnectionTestActivity extends AppCompatActivity {

    public static BroadcastReceiver receiver = null;
    public static Timer timer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_test);
        MainActivity.connectionTest = new WeakReference<>(this);
        if (receiver != null)
            registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void btn_cancel_Click(View v) {
        if (timer != null)
            timer.cancel();
        if (receiver != null)
            try {
                unregisterReceiver(receiver);
            } catch (IllegalArgumentException e) {
                // in case receiver is already unregistered
                // can happen if receiver was registered from another context
            }
        timer = null;
        receiver = null;
        this.finish();
    }
}
