App zur sicheren Konfiguration von Eduroam
==========================================

Funktionsweise
--------------

* App startet
* Nutzer fixt ggf Vorbedingungen wie fehlender Sperrbildschirm
* Nutzer authentifiziert sich am WTC
* App holt sich im IdM ein App- und Gerätepasswort und bekommt die Konfiguration als JSON zurück
* App prüft per JSON API, ob Ressource deployed ist
* App konfiguriert eduroam

Release bauen
-------------

* Release apk wird immer vom CI runner mit gebaut
* Artifacts herunterladen
* Artifacts nach /tmp auspacken
* /afs/tu-chemnitz.de/global/capp/AndroidStudio/SDK/build-tools/26.0.1/apksigner sign --ks ~/Tuc-Eduroamcat.jks --out my-app-release.apk /tmp/app/build/outputs/apk/app-release-unsigned-aligned.apk

Checkliste Release veröffentlichen
----------------------------------

* Ist die Versionsnummer in app/build.gradle erhöht worden?
  * versionCode 
  * versionName
* Stimmt die signatur überein
* APK nach /afs/tu-chemnitz.de/www/root/urz/network/access/apks/ kopieren, Link TUCWLAN-release-latest.apk anpassen
* Im Playstore veröffentlichen, Release Notes schreiben